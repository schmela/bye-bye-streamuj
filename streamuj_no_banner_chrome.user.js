// ==UserScript==
// @name           Streamuj.cz - No Banners for Chrome
// @version        1.0
// @author         schmela
// @namespace      streamuj.cz
// @description    posune vytvoreni banneru na hodne daleko do budoucnosti :D
// @include        http://www.streamuj.tv/video/*
// @grant          none
// ==/UserScript==

// -------------------------------------------------
// script odstrani puvodni spatny JW Player s reklamou
// najde jeho vytvareci script a upravi ho tak, aby se reklama spustila za hodne dlouho
// upraveny script pak spusti
// --------------------------------------------------

window.onload=function() {
    
  // smazem puvodni JW Player  
  jwplayer("mediaplayer").remove();  
   
  // podle tohohle regexpu pozname nas script  
  var re = /.*ukazReklamu.*/mig;
    
  // vsecky scripty  
  var scripts = document.getElementsByTagName("script");
    
  // prochazime vsechny scripty  
  for (var i=0;i<scripts.length;i++) {
     
      // pokud najdem nas script
     if (scripts[i].innerHTML.search(re) != -1) {   

      //console.log(scripts[i].innerHTML);
         
      // zamenime dobu za jakou se vytvori banner z 60s na moc :D
      var skrypt = scripts[i].innerHTML.replace("gatespots: 60,","gatespots: 999999,").replace("ukazReklamu","ukaaaaazReklamu");
       
      // spustime upraveny script (ktery znovuvytvori JW Player) 
      eval (skrypt);          
        
      break;
     
   }
     
  }
}