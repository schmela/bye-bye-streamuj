// ==UserScript==
// @name        Streamuj.cz - Tlačítka
// @version     1.0
// @author      schmela
// @namespace   streamuj.cz
// @description zobrazi tlacitko ktery da dopice banner na zaplaceni
// @include     http://www.streamuj.tv/video/*
// @grant       GM_addStyle
// ==/UserScript==

var zNode;
CreateButtons();


//  vytvori div a v nem tlacitka pro download nastylovana pomoci CSS
function CreateButtons() {

    // vytvor <div id="myButtons">
    zNode       = document.createElement ('div');
    zNode.setAttribute ('id', 'myButtons');

    // pridej do <div> tlacitko na skryti
    zNode.innerHTML = '<button id="skryjButton" type="button">Dopice s placenim!</button>';        
    document.getElementById ("skryjButton").addEventListener (
        "click", SkryjButtonClickAction, false
    );

    // prida <div> do stranky
    document.body.appendChild (zNode);        
}

function SkryjButtonClickAction (zEvent) {    
  
    // skryje banner
    unsafeWindow.skryjBranu();
    
    // skryje tlacitka
    var buttons = document.getElementById('myButtons');
    buttons.style.visibility = 'hidden';    
}

//--- Style our newly added elements using CSS.
GM_addStyle ( multilineStr ( function () {/*!
    #myButtons {
        position:               absolute;
        top:                    0;
        left:                   0;
        font-size:              20px;
        background:             orange;
        border:                 3px outset black;
        margin:                 5px;
        opacity:                0.9;
        z-index:                222;
        padding:                5px 20px;
    }
    #skryjButton {
        cursor:                 pointer;
    }
    #myButtons p {
        color:                  red;
        background:             white;
    }
*/} ) );

function multilineStr (dummyFunc) {
    var str = dummyFunc.toString ();
    str     = str.replace (/^[^\/]+\/\*!?/, '') // Strip function () { /*!
            .replace (/\s*\*\/\s*\}\s*$/, '')   // Strip */ }
            .replace (/\/\/.+$/gm, '') // Double-slash comments wreck CSS. Strip them.
            ;
    return str;
}



