// ==UserScript==
// @name           Streamuj.cz - No Banners
// @version        1.0
// @author         schmela
// @namespace      streamuj.cz
// @description    posune vytvoreni banneru na hodne daleko do budoucnosti :D
// @include        http://www.streamuj.tv/video/*
// @grant          none
// @run-at         document-start
// ==/UserScript==


// pred spustenim kazdeho scriptu (e) se spusti tohle
// hleda se script ve kterem je obsazen string "ukazReklamu"
// tomu se zakaze spusteni
// potom pridame ten samy akorat drobne upraveny
// aby se vse rekurzivne neopakovalo, tak se v novem scriptu nesmi objevit "ukazReklamu"
window.addEventListener('beforescriptexecute', function(e) {
  
   // hleda se script ve krerem je obsazen string "ukazReklamu"
   var re = /.*ukazReklamu.*/mig;
   if (e.target.innerHTML.search(re) != -1) {   

      // zamenime dobu za jakou se vytvori banner z 60s na moc :D
      var skrypt = e.target.innerHTML.replace("gatespots: 60,","gatespots: 999999,").replace("ukazReklamu","ukaaaaazReklamu");
        
      // zastavime spusteni puvodniho scriptu
      e.stopPropagation();
      e.preventDefault();
      
      // pridame HTML kod upraveneho scriptu do stranky
      // + z nej jeste smaznem vsechny funkce
      document.head.appendChild(document.createElement('script'))
           .innerHTML = skrypt;
     
   }          
//   else {
//      console.log(document.scripts.length + e.target.innerHTML);
//   }  
          
   // remove the listener:
   // window.removeEventListener(e.type, arguments.callee, true);

}, true);

